class Reserva < ActiveRecord::Base
belongs_to :user
validates :numpiso, :presence => true
validates :user_id, :presence => true, :uniqueness => true
end
