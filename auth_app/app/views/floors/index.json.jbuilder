json.array!(@floors) do |floor|
  json.extract! floor, :id, :piso, :puestos, :ocupados, :disponibles
  json.url floor_url(floor, format: :json)
end
