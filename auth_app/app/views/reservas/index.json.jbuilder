json.array!(@reservas) do |reserva|
  json.extract! reserva, :id, :numpiso, :fecha, :horas, :user_id
  json.url reserva_url(reserva, format: :json)
end
