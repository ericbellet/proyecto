class ApplicationMailer < ActionMailer::Base
  default from: "rafael.pina@estacionamiento.com"
  layout 'mailer'

  def email_actualizacion(user)
   	@user=user
   	mail(to: @user.email, subject: 'Cuenta Actualizada Satisfactoriamente!!')
  end

  def cuenta_creada(user)
  	@user=user
  	mail(to: @user.email, subject: 'Bienvenido')
  end

  def reserva_exitosa(reserva)
    @reserva=reserva
    mail(to: @reserva.user.email, subject: 'Tu Reserva ya esta disponible')
  end

  def factura(reserva)
    @reserva=reserva
    mail(to: @reserva.user.email, subject: 'Factura de Cobro')
  end
end
