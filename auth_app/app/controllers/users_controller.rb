class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :validar_admin, only: [:index]
  before_action :validar_operator, only: [:destroy,:update]
  # GET /users
  # GET /users.json
  def index
    @users = User.all
    if session[:user_id]==2
      redirect_to reservas_path()
    else
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])


    respond_to do |format|
      format.html
      format.json { render json: @user }
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    if @user=User.update(user_params)
      Appmailer.cuenta_creada(@user).deliver
      flash[:notice]="Actualizado Con exito"
    else
      redirect_to home_sessions_path()
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    Appmailer.cuenta_creada(@user).deliver
    if @user.save
      flash[:notice] = "You Signed up successfully"
      flash[:color]= "valid"
    else
      flash[:notice] = "Form is invalid"
      flash[:color]= "invalid"
    end
    render "new"
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy

    if @user.id == 1
      flash[:notice] = "Imposible delete ADMIN"
      redirect_to administrar_cuentas_sessions_path()
    elsif @user.id == 2
        flash[:notice] = "Imposible delete OPERATOR"
      redirect_to administrar_cuentas_sessions_path()
    else
       @user.destroy   
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end

    
  end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation)
    end

    def validar_admin
      unless session[:user_id]==1 or session[:user_id]==2
        redirect_to home_sessions_path()
      end
    end

    def validar_operator
      if session[:user_id]==2
        redirect_to home_sessions_path()
      end
    end
end
