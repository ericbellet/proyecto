class SessionsController < ApplicationController
 
	before_filter :authenticate_user, :except => [:index, :login, :login_attempt, :logout]
	before_filter :save_login_state, :only => [:index, :login, :login_attempt]

	def home
		
	end

	def profile
		#Profile Page
	end

	def setting
		#Setting Page
	end

	def login
		#Login Form
	end

	def administrar_puestos

	end

	def login_attempt
		authorized_user = User.authenticate(params[:username_or_email],params[:login_password])
		if authorized_user
			if session[:user_id] == 1
				flash[:notice] = "HI ADMIN"
				session[:user_id] = authorized_user.id
				redirect_to home_sessions_path()
			elsif session[:user_id] == 2
				flash[:notice] = "HI OPERADOR"
				session[:user_id] = authorized_user.id
				redirect_to home_sessions_path()

			else
			session[:user_id] = authorized_user.id
			redirect_to home_sessions_path()
		end

		else
			flash[:notice] = "Invalid Username or Password"
        	flash[:color]= "invalid"
			render "login"	
		end
	end

	def logout
		session[:user_id] = nil
		redirect_to :action => 'login'
	end

end