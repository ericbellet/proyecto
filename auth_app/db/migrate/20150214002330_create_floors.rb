class CreateFloors < ActiveRecord::Migration
  def change
    create_table :floors do |t|
      t.integer :piso
      t.integer :puestos
      t.integer :ocupados
      t.integer :disponibles

      t.timestamps null: false
    end
  end
end
