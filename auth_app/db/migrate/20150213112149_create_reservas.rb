class CreateReservas < ActiveRecord::Migration
  def change
    create_table :reservas do |t|
      t.integer :numpiso
      t.date :fecha
      t.integer :horas
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
